package com.admin.server.adminserver1;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import de.codecentric.boot.admin.config.EnableAdminServer;


@EnableAdminServer
@SpringBootApplication
public class AdminServer1Application {
	
	

	public static void main(String[] args) {
		SpringApplication.run(AdminServer1Application.class, args);
	}

}
