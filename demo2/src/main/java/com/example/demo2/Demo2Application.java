package com.example.demo2;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@SpringBootApplication
public class Demo2Application {

	@RestController
	@RefreshScope
	class ExampleController {

		@RequestMapping("/demo2")
		String home() {
			return "Hello demo 2.x";
		}

		@Value("${user.role}")
		private String role;
		
		@Value("${user.common}")
		private String common;
		
		

		@RequestMapping(value = "/whoami/{username}", method = RequestMethod.GET, produces = MediaType.TEXT_PLAIN_VALUE)
		public String whoami(@PathVariable("username") String username) {
			return String.format("Demo 2 - %s --- %s --- %s ", username, role, common );
		}

	}

	public static void main(String[] args) {
		SpringApplication.run(Demo2Application.class, args);
	}

}
